<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class Bing
{
    var $fileTypes = [
        '1920x1200',
        '1920x1080',
        '1366x768',
        '1280x768',
        '1024x768',
        '800x600',
        '800x480',
        '768x1280',
        '720x1280',
        '640x480',
        '480x800',
        '400x240',
        '320x240',
        '240x320'
    ];

    /**
     * 获取必应的壁纸(国际版)
     * @param $idx : 今天为0，昨天为1，前天为2，以此类推
     * @param $num : 多少条记录
     * @return array
     */
    public function getLatestPic($idx = 0, $num = 1)
    {
        $url = 'https://www.bing.com/HPImageArchive.aspx';
        $params = [
            'format' => 'js',
            'idx' => $idx,
            'n' => $num
        ];
        $result = Http::withHeaders([
            'cookie' => 'ENSEARCH=BENVER=1'
        ])->get($url, $params);
        $items = [];
        if (isset($result['images'])) {
            $items = $result['images'];
        }
        $params = [];
        foreach ($items as $item) {
            $param = [
                'startdate' => $item['startdate'],
                'fullstartdate' => $item['fullstartdate'],
                'enddate' => $item['enddate'],
                'url' => 'https://www.bing.com' . $item['url'],
                'copyright' => $item['copyright'],
                'title' => $item['title'],
                'hsh' => $item['hsh'],
            ];
            $params []= $param;
        }
        return $params;
    }

    /**
     * 获取必应的壁纸(国内版)
     * @param $idx : 今天为0，昨天为1，前天为2，以此类推
     * @param $num : 多少条记录
     * @return array
     */
    public function getLatestPicCN($idx = 0, $num = 1)
    {
        $url = 'https://cn.bing.com/HPImageArchive.aspx';
        $params = [
            'format' => 'js',
            'idx' => $idx,
            'n' => $num
        ];
        $result = Http::withHeaders([
            'cookie' => 'ENSEARCH=BENVER=0'
        ])->get($url, $params);
        $items = [];
        if (isset($result['images'])) {
            $items = $result['images'];
        }
        $params = [];
        foreach ($items as $item) {
            $param = [
                'startdate' => $item['startdate'],
                'fullstartdate' => $item['fullstartdate'],
                'enddate' => $item['enddate'],
                'url' => 'https://cn.bing.com' . $item['url'],
                'copyright' => $item['copyright'],
                'title' => $item['title'],
                'hsh' => $item['hsh'],
            ];
            $params []= $param;
        }
        return $params;
    }

    public function download($url)
    {
        $arr = explode('?', $url)[1];
        $params = explode('&', $arr);
        $fileName = '';
        foreach ($params as $param) {
            $keyVal = explode('=', $param);
            if ($keyVal[0] == 'id') {
                $fileName = $keyVal[1];
                $fileName = str_replace('OHR.', '', $fileName);
                break;
            }
        }
        $path = '/bing/' . date('Y') .'/' . $fileName;
        // 下载1920x1080
        $rsp = Http::get($url);
        Storage::put($path, $rsp);
        // if(strpos($fileName,'1920x1080') !== false){
            // 下载所有格式
            // $path = '/bing/1920x1080/' . $fileName;
            // foreach ($this->fileTypes as $fileType) {
            //     $tmpUrl = str_replace('1920x1080', $fileType, $url);
            //     $tmpPath = str_replace('1920x1080', $fileType, $path);
            //     $rsp = Http::get($tmpUrl);
            //     Storage::put($tmpPath, $rsp);
            // }
        // }else{
        //     debugger('必应图片下载出错', __FILE__, __LINE__);
        //     debugger($url, __FILE__, __LINE__);
        // }
        return $path;
    }
}
