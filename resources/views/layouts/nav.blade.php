<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <a class="navbar-brand" href="/">
        <img src="{{ URL::asset('images/logo.svg') }}" width="30" height="30" alt="" loading="lazy">
        <span>Wallpaper</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        {{--<form class="form-inline">--}}
        {{--    <input class="form-control" type="search" placeholder="搜索">--}}
        {{--    <span class="nav-search">--}}
        {{--        <i class="fa fa-search"></i>--}}
        {{--    </span>--}}
        {{--</form>--}}
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">首页</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">关于</a>
            </li>
        </ul>
    </div>
</nav>
