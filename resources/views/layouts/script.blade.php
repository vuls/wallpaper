
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ URL::asset('frameworks/jquery/jquery.min.js') }}"></script>
{{-- 瀑布流图片展示插件 --}}
<script src="{{ URL::asset('frameworks/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ URL::asset('frameworks/masonry.pkgd.min.js') }}"></script>
<script src="{{ URL::asset('frameworks/infinite-scroll.pkgd.min.js') }}"></script>
<script src="{{ URL::asset('frameworks/popper.min.js') }}"></script>
<script src="{{ URL::asset('frameworks/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('frameworks/download.js') }}"></script>
<script src="{{ URL::asset('dist/js/index.js') }}"></script>
